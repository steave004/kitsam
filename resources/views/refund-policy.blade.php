<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Refund Policy || Kitsam </title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/kt.png" rel="icon">
    <link href="assets/img/kt.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style1.css" rel="stylesheet">

</head>

<body>

<!-- ======= Header ======= -->
<header id="header">
    <div class="container d-flex align-items-center justify-content-between">

        <div class="logo">
            <h1><a href="/">Kitsam<span></span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="/#hero">Home</a></li>
                <li><a class="nav-link scrollto" href="/#features">Features</a></li>
                <li><a class="nav-link scrollto" href="/#how_to_play">How to play</a></li>
                <li><a class="nav-link scrollto" href="/#achievements">Achievements</a></li>
                <li><a class="nav-link scrollto" href="/#contact">Contact</a></li>
                <li><a class="getstarted scrollto" href="/#hero">Get Started</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header><!-- End Header -->
<section  class="features section-bg" style="background:#f1f6fe">
    <div class="container">
        <div class="section-title">
            <h2 data-aos="fade-in" class="aos-init aos-animate">Refund Policy</h3>
        </div>
    </div>
</section>

<main id="main">
    <section class="sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="para1">
                        <p>All deposits and purchases made and fees paid on Kitsam.com to play or in connection with Fantasy. All transactions are final. We recognize that customer satisfaction is extremely important to us, so only error-oriented transaction cases shall be reviewed. </p>
                        <p>If you are not fully satisfied with any deposit/purchase made or fee paid on Kitsam and feel there is an 'Error’ in the transaction, please let us know about that from the transaction date, and we shall certainly review the transaction and determine the resolution at our own discretion. </p>
                        <p>Kitsam reserves the right to cancel any matches for any reason, with or without disclosure of the reason, and refund all fees paid for such matches immediately by any and all users. </p>
                        <p class="text-black-50">By email: support@kitsam.com</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-top">
        <div class="container">
            <div class="row  justify-content-center">
                <div class="col-lg-6">
                    <h3>KITSAM</h3>
                    <p>KITSAM is one of the most popular fantasy sports and fantasy cricket platforms all across the world. Our platform is one of the simplest to play on and the easiest ways to win real cash.</p>
                </div>
            </div>

            <div class="useful mt-5">
                <h4>Useful Links</h4>
                <div class="opt-inline">
                    <a href="/privacy-policy" class="pd">Privacy Policy</a><span>||</span>
                    <a href="/terms-of-use" class="pd">Terms of Use</a><span>||</span>
                    <a href="/refund-policy" class="pd">Refund Policy</a>
                </div>
            </div>
            <div class="social-links">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
        </div>
    </div>

    <div class="container footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>Kitsam</span></strong> All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bocor-bootstrap-template-nice-animation/ -->
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

</body>

</html>
