<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Terms-Of-Use || Kitsam </title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/kt.png" rel="icon">
    <link href="assets/img/kt.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style1.css" rel="stylesheet">

</head>

<body>

<!-- ======= Header ======= -->
<header id="header">
    <div class="container d-flex align-items-center justify-content-between">

        <div class="logo">
            <h1><a href="/">Kitsam<span></span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="/#hero">Home</a></li>
                <li><a class="nav-link scrollto" href="/#features">Features</a></li>
                <li><a class="nav-link scrollto" href="/#how_to_play">How to play</a></li>
                <li><a class="nav-link scrollto" href="/#achievements">Achievements</a></li>
                <li><a class="nav-link scrollto" href="/#contact">Contact</a></li>
                <li><a class="getstarted scrollto" href="/#hero">Get Started</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header><!-- End Header -->

<section  class="features section-bg" style="background:#f1f6fe">
    <div class="container">
        <div class="section-title">
            <h2 data-aos="fade-in" class="aos-init aos-animate">Terms of Use</h2>
        </div>
    </div>
</section>

<main id="main">
    <section class="sec">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 pt-5 pt-lg-0">
                    <div class="policy">

                        <h3 class="pt-2 pb-2">Agreement</h3>
                        <p>We advise you to read our Terms of Service carefully and understand them well. All the clauses in the Terms of Service of Kitsam are delineated below and included in the medium of reference that can be accessed by clicking on the related links. You should make sure that you have read and understood our Agreement, including our Terms of Service, well before becoming a member of Kitsam. By registering on KItsam, you agree to follow our Terms of Service, Privacy Policy, and Game Rules. You are completely bound by our Agreement in any instance when you choose to use our Services or Games or software. We reserve the right to modify and amend our Terms of Service at any time. When we make any changes to our Terms of Service, we shall inform you about the revisions by sending you a notice at your email address registered with us. We shall also update such changes on our Website.</p>
                        <p>We may make changes to our Terms of Service in order to review or change any of our Policies and Game Rules. If you do not agree to the changes made, you may cease your use of our Services of online Fantasy games by informing us that you have deleted your account. If you continue to use our Services after any amendments or changes, it shall be assumed that you have accepted the changes made in the Agreement and you agree to our new Terms.</p>

                        <h3 class="pt-2 pb-2">Warranty </h3>
                        <p>Kitsam aims to provide information that is accurate, complete, and reliable at the time of publishing or posting it. However, Kitsam makes no representations about the correctness, completeness, or suitability of any information published on its Website or contained in its documents. All the information and related graphics are provided on an "as is" basis without warranty of any kind, express or implied. Kitsam hereby disclaims all warranties with regard to such information, including all implied warranties and conditions of merchantability, fitness for a particular purpose, title, and non-infringement. Regardless of Kitsam’s efforts to provide its users with the Services of the highest quality, Kitsam does not provide Users with any warranty or representation whatsoever regarding the quality, fitness for purpose, completeness, merchantability, non-infringement, or accuracy of the Software, the Website and the Fantasy Cricket games. Kitsam makes no warranty that its Software, Website, Games, or Servers shall be uninterrupted, timely, or error-free, that defects shall be corrected, or that the features and Services provided therein shall be free from viruses or bugs of any kind. Kitsam reserves the right to determine, at its sole discretion, whether it is responsible for any malfunction or disruption. Kitsam reserves the right to limit your tournament/game participation or terminate your account in case it determines that you have intentionally caused such a malfunction or disruption. Kitsam is not liable for any potential winnings from any unfinished tournaments or unfinished online Fantasy Cricket games.</p>

                        <h3 class="pt-2 pb-2">Use of Software</h3>
                        <p>Commercial use of our Software is strictly forbidden. Users are only allowed to use the Software for their personal entertainment. Under no circumstances shall Users be permitted to use their accounts on Kitsam .com for any purpose other than playing the games for entertainment. Users may not attempt to modify, decompile, reverse-engineer, or disassemble the Software in any way. The use of artificial intelligence including, without limitation, robots is strictly forbidden in connection with the Software and the Games. All actions taken in relation to Fantasy Cricket games by Users must be executed personally by Users using the user interface accessible through the Software.</p>

                        <h3 class="pt-2 pb-2">Your Password, Your Responsibility</h3>
                        <p>After you have registered on our Website, it is solely your responsibility to keep your password absolutely confidential. Make sure you do not share your password with anybody, be it your family, friends, or anyone else. We are not responsible in any way if your user account is logged in or accessed by anybody else using your password.</p>

                        <h3 class="pt-2 pb-2">Breach and Consequences</h3>
                        <p>In the event of a breach of any of our Terms of Service, evidenced by our investigation, or if there is a reasonable belief that your continued access to our Website is detrimental to the interests of Kitsam Fantasy Cricket, our other Users, or the general public, we may, at our sole discretion, take any or all of the following actions:</p>
                        <p>1. Restrict games between users suspected of colluding or cheating;</p>
                        <p>2. Permanently freeze your user account on the Website;</p>
                        <p>3. Seize the money in your user account;</p>
                        <p>4. Demand damages for breach and take appropriate civil action to recover such damages, and/or initiate prosecution for violations that amount to offenses in law.</p>
                        <p>5. Additionally, in the event of a material breach thereof, we reserve the right to bar you from future registration on the Website.</p>
                        <p>The decision of Kitsam Management on the action to be taken as a consequence of breach shall be final and binding on you. Any action taken by the KitsamFantasy Cricket platform shall be without prejudice to our other rights and remedies available in law or equity.</p>

                        <h3 class="pt-2 pb-2">Governing Law, Dispute Resolution, and Jurisdiction</h3>
                        <p>This Agreement shall in all respects be governed and interpreted and construed in accordance with the laws of India. All disputes, differences, complaints, etc. shall be subject to arbitration under the Indian Arbitration and Conciliation Act, 1996.</p>

                        <h3 class="pt-2 pb-2">Content</h3>
                        <p>All content and material on the Website, including, but not limited to, information, images, marks, logos, designs, pictures, graphics, text content, hyperlinks, multimedia clips, animation, games, and software (collectively referred to as "Content" hereinafter), whether or not belonging to Kitsam, are protected by applicable intellectual property laws. Additionally, all chat content, messages, images, recommendations, emails, images sent by any Users can be logged/recorded by us and shall form part of Content and the KitsamFantasy platform is free to use this material in any manner whatsoever.</p>
                        <p>Our Website may contain information about or hyperlinks to third parties. In such cases, we are not in any way responsible for the content on such websites, and we do not provide any express or implied warranty of the accuracy, completeness, suitability, or quality of the content belonging to such third-party websites. You may choose to rely on any third-party content (content that does not belong to Kitsam) posted on our Website solely at your own risk and liability.</p>
                        <p>If you visit a third-party website through any third-party content posted on Kitsam.com, you will be subject to the terms and conditions applicable to that content. We neither control nor are we responsible for the content published on such third-party websites. The existence of a link to a third-party website on our Website is not in any way an endorsement of that website by us.</p>


                        <h3 class="pt-2 pb-2">Indemnity</h3>
                        <p>To the extent permitted by law, and in consideration for being allowed to use our Services and participate in our Contests, you hereby agree to indemnify, save and hold harmless and defend us (to the extent of all benefits and awards, cost of litigation, disbursements and reasonable attorney's fees that we may incur in connection therewith including any direct, indirect or consequential losses, any loss of profit and loss of reputation) from any claims, actions, suits, taxes, damages, injuries, causes of action, penalties, interest, demands, expenses and/or awards asserted or brought against us by any person in connection with:</p>
                        <p>1. Infringement of their intellectual property rights by your publication of any Content on our Website;</p>
                        <p>2. defamatory, offensive or illegal conduct of any other player or for anything that turns out to be misleading, inaccurate, defamatory, threatening, obscene or otherwise illegal whether originating from another player or otherwise;</p>
                        <p>3. Use, abuse, or misuse of your user account on our online Fantasy  Website in any manner whatsoever;</p>
                        <p>4. any disconnections, technical failures, system breakdowns, defects, delays, interruptions, manipulated or improper data transmission, loss or corruption of data or communication lines failure, distributed denial of service attacks, viruses, or any other adverse technological occurrences arising in connection with your access to or use of our Website; and</p>
                        <p>5. Access your user account by any other person accessing the Services using your username or password, whether or not with your authorization.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-top">
        <div class="container">
            <div class="row  justify-content-center">
                <div class="col-lg-6">
                    <h3>KITSAM</h3>
                    <p>KITSAM is one of the most popular fantasy sports and fantasy cricket platforms all across the world. Our platform is one of the simplest to play on and the easiest ways to win real cash.</p>
                </div>
            </div>

            <div class="useful mt-5">
                <h4>Useful Links</h4>
                <div class="opt-inline">
                    <a href="/privacy-policy" class="pd">Privacy Policy</a><span>||</span>
                    <a href="/terms-of-use" class="pd">Terms of Use</a><span>||</span>
                    <a href="/refund-policy" class="pd">Refund Policy</a>
                </div>
            </div>

            <div class="social-links">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>

        </div>
    </div>

    <div class="container footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>Kitsam</span></strong> All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bocor-bootstrap-template-nice-animation/ -->
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

</body>

</html>
