<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Kitsam - Index</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/kt.png" rel="icon">
    <link href="assets/img/kt.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style1.css" rel="stylesheet">

</head>

<body>

<!-- ======= Header ======= -->
<header id="header">
    <div class="container d-flex align-items-center justify-content-between">

        <div class="logo">
            <h1><a href="/">Kitsam<span></span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
                <li><a class="nav-link scrollto" href="#features">Features</a></li>
                <li><a class="nav-link scrollto" href="#how_to_play">How to play</a></li>
                <li><a class="nav-link scrollto" href="#achievements">Achievements</a></li>
                <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
                <li><a class="getstarted scrollto" href="#hero">Get Started</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero">
    <div class="container">
        <div class="row d-flex align-items-center">
        <div class=" col-lg-6 py-5 py-lg-0 order-2 order-lg-1" data-aos="fade-right">
            <h1>Create Your Team
                And Win Real Cash</h1>
            <h2>This fantasy cricket app allows you to win real cash by creating your team with your skills and performanance of players on the ground.</h2>
            <a href="#about" class="btn-get-started scrollto">Get Started</a>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left">
            <img src="assets/img/main.svg" class="img-fluid" alt="" style="margin-top: 70px" draggable="false">
        </div>
    </div>
    </div>
</section>
<!-- End Hero -->

<main id="main">
    <!-- ======= Features Section ======= -->
    <section id="features" class="features section-bg">
        <div class="container">
            <div class="section-title">
                <h2 data-aos="fade-in">Features</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="box" data-aos="fade-in">
                        <div class="icn"><i class="bx bx-money"></i></div>
                        <div class="bx-title">
                            <h2 class="mt-3">Fast Withdrawal</h2>
                            <p class="mt-2">Your withdrawal request will be processed as soon as you request your withdrawal after your bank verification.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box" data-aos="fade-in">
                        <div class="icn"><i class="bx bx-user-plus"></i></div>
                        <div class="bx-title">
                            <h2 class="mt-3">Refer & Earn</h2>
                            <p class="mt-2">Refer game to your friends and get referral bonus which can be used within the App to join any league.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box" data-aos="fade-in">
                        <div class="icn"><i class="bx bx-lock-alt"></i></div>
                        <div class="bx-title">
                            <h2 class="mt-3">Understandable Apps</h2>
                            <p class="mt-2">kitsam is provides Online betting system where you can play with your friends worldwide.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Features Section -->

    <!-- ======= How to Play Section ======= -->
    <section id="how_to_play" class="about section-bg">
        <div class="container">
            <div class="section-title">
                <h2 data-aos="fade-in">How to Play</h2>
            </div>
            <div class="row">
                <div class="col-md-7 offset-md-1">
                    <div class="contain" data-aos="fade-in">
                        <div class="feature">
                            <img src="assets/img/h-icon1.svg" alt="" draggable="false">
                            <div class="inner-txt">
                                <h2>Choose a CONTEST</h2>
                                <p>There are hundreds available every day</p>
                            </div>
                        </div>
                        <div class="feature">
                            <img src="assets/img/h-icon2.svg" alt="" draggable="false">
                            <div class="inner-txt">
                                <h2>DRAFT YOUR TEAM</h2>
                                <p>Fill your lineup without going over the salary cap</p>
                            </div>
                        </div>
                        <div class="feature">
                            <img src="assets/img/h-icon3.svg" alt="" draggable="false">
                            <div class="inner-txt">
                                <h2>WATCH AND WIN</h2>
                                <p>Track your score in real time and win real cash</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="img-pos" data-aos="fade-left">
                        <img src="assets/img/h-graphic.svg" alt="" draggable="false">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End How to Play Section -->

    <!-- ======= Our Achievements Section ======= -->
    <section id="achievements" class="services section-bg">
        <div class="container">
            <div class="section-title">
                <h2 data-aos="fade-in">Our Achievements</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="achivement achivement1" data-aos="fade-in">
                        <div class="achivement_inner">
                            <h4>500k +</h4>
                            <p>Total Winnings</p>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="achivement achivement2" data-aos="fade-in">
                        <div class="achivement_inner">
                            <h4>1.7k +</h4>
                            <p>App Downloads</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="achivement achivement3" data-aos="fade-in">
                        <div class="achivement_inner">
                            <h4>50k +</h4>
                            <p>Total Reviews</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Achievements Section -->

    <!-- ======= Download Section ======= -->
    <section id="" class="team section-b download_app">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-md-12">
                    <div class="download_app_block">
                        <div class="download_app_image">
                            <img src="assets/img/download.png" alt="" class="img-fluid">
                        </div>
                        <h2>Download</h2>
                        <h3 class="animated fadeIn">the Kitsam app now!</h3>
                        <p>Create team, Join contests &amp; win exciting cash prizes.</p>
                        <div class="mt-5">
                            <a href="/" class=" mt-3 mx-2">
                                <img src="assets/img/android.svg" alt="" class="img-fluid" style="height:70px;">
                            </a>
                            <a href="/" class=" mt-3 mx-2">
                                <img src="assets/img/apple.svg" alt="" class="img-fluid" style="height:70px;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Download  Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Contact</h2>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="info-box" data-aos="fade-up">
                                <i class="bx bx-map"></i>
                                <h3>Our Address</h3>
                                <p> 68/7, B. SUNDAR NAGAR, <br> NEAR MAHADEVA TARI, HATHI PAHAR, BANDHA, KARNIBAGH, KUNDA,
                                    <br> DEOGHAR Deoghar JH 814143</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box mt-4" data-aos="fade-up" data-aos-delay="100">
                                <i class="bx bx-envelope"></i>
                                <h3>Email Us</h3>
                                <p>support@kitsam.com</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box  mt-4" data-aos="fade-up" data-aos-delay="100">
                                <i class="bx bx-phone-call"></i>
                                <h3>Call Us</h3>
                                <p>+91 9874828488</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 mt-4 mt-lg-0">
                    <form action="forms/contact.php" method="post" role="form" class="php-email-form w-100" data-aos="fade-up">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                            </div>
                            <div class="col-md-6 form-group mt-3 mt-md-0">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                        </div>
                        <div class="my-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center"><button type="submit">Send Message</button></div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Section -->

</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-top">
        <div class="container">
            <div class="row  justify-content-center">
                <div class="col-lg-6">
                    <h3>KITSAM</h3>
                    <p>KITSAM is one of the most popular fantasy sports and fantasy cricket platforms all across the world. Our platform is one of the simplest to play on and the easiest ways to win real cash.</p>
                </div>
            </div>
            <div class="useful mt-5">
                <h4>Useful Links</h4>
                <div class="opt-inline">
                    <a href="/privacy-policy" class="pd">Privacy Policy</a><span>||</span>
                    <a href="/terms-of-use" class="pd">Terms of Use</a><span>||</span>
                    <a href="/refund-policy" class="pd">Refund Policy</a>
                </div>
            </div>

            <div class="social-links">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
        </div>
    </div>

    <div class="container footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>Kitsam</span></strong> All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bocor-bootstrap-template-nice-animation/ -->
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

</body>

</html>
