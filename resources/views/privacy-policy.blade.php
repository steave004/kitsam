<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Privacy Policy || Kitsam </title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/kt.png" rel="icon">
    <link href="assets/img/kt.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style1.css" rel="stylesheet">

</head>

<body>

<!-- ======= Header ======= -->
<header id="header">
    <div class="container d-flex align-items-center justify-content-between">

        <div class="logo">
            <h1><a href="/">Kitsam<span></span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="/#hero">Home</a></li>
                <li><a class="nav-link scrollto" href="/#features">Features</a></li>
                <li><a class="nav-link scrollto" href="/#how_to_play">How to play</a></li>
                <li><a class="nav-link scrollto" href="/#achievements">Achievements</a></li>
                <li><a class="nav-link scrollto" href="/#contact">Contact</a></li>
                <li><a class="getstarted scrollto" href="/#hero">Get Started</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header><!-- End Header -->

<section  class="features section-bg" style="background:#f1f6fe">
    <div class="container">
        <div class="section-title">
            <h2 data-aos="fade-in" class="aos-init aos-animate">Privacy Policy</h2>
        </div>
    </div>
</section>

<main id="main">
    <section class="sec">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 pt-5 pt-lg-0">
                    <div class="policy">

                        <h3 class="pt-2 pb-2">Our Privacy Is Important</h3>

                        <p>
                            We provide online Fantasy games on Kitsam for our Users’ entertainment in a safe, secure, and trustworthy environment. We are committed to protecting the privacy of our Users. This Privacy Policy explains what information we collect from our users, how we use their information, and what choices are available to our Users in this regard.
                        </p>
                        <p>
                            This Privacy Policy forms part of our Website’s Terms of Service. Kitsam respects your privacy and assures you that any information provided by you to Kitsam is fully protected and will be used according to this Privacy Policy. To play games of Fantasy offered on Kitsam, you may have to provide certain information to us.
                        </p>

                        <h3 class="pt-2 pb-2">The Information We Collect</h3>

                        <p>
                            You are required to register with us to become our User and access our games. We collect the personal information of our Users. Examples of this include Users’ names, addresses, email addresses, and phone numbers. If you do not provide us with the required information, you may not be able to access our Games and other Services. We also collect information other than your personal information, such as your IP address. Such information is not associated with your personal information and cannot be linked to you personally. We may use IP addresses to analyze trends, administer the Website, track Users’ movement and gather broad demographic information for aggregate use. IP addresses are not linked to personally identifiable information.
                            <p>
                             Kitsam.com is the sole owner of the information collected on this Website. We will not sell, share, or rent our Users’ information to others in ways different from what is disclosed in this Privacy Policy.
                          </p>
                        </p>

                        <h3 class="pt-2 pb-2">Usage of the Information Collected</h3>

                        <p>
                            Kitsam does not disclose your personal information to any third party without your express permission. However, by registering on our Website or App, you grant us permission to use and share your personal information as stated in this Privacy Policy. We may need to disclose your information when required by law. We use your personal information to improve our Services and enhance your experience on our Website as well as to notify you of our new Fantasy game offers, Products, and Services. Your personal information may be shared with the sponsors and suppliers of prizes for our winners.
                        </p>
                        <p>
                            Kitsam uses a foolproof security system to prevent the loss, misuse, and alteration of User information. Your registration data is password-protected and only you can access your information.
                           <p>
                                Kitsam is not responsible for any activity that takes place in your account when your password is used. We strongly advise you not to disclose your password to anyone whosoever.
                            </p>
                        </p>

                        <h3 class="pt-2 pb-2">Cookies</h3>

                        <p>
                            When you visit our Website or use our App, cookies may be left on your computer or phone. A cookie is a small text file that uniquely identifies your browser and is assigned by the servers of Kitsam.com. It may be used to personalize your experience on Kitsam.com.
                           <p>
                                Additionally, it may be used for authentication, game management, and security purposes. A cookie in no way gives us access to your computer or any information about you other than the data you choose to share with us.
                            </p>
                        </p>
                        <p>
                            Cookies may also be assigned by the advertisers of Kitsam when you click on any of the advertisements shown on Kitsam.com. In such an event, the cookies are controlled by these advertisers and not by us.
                        </p>
                        <p>
                            You can choose to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you wish. This may prevent you from taking full advantage of our Products and Services.
                        </p>

                        <h3 class="pt-2 pb-2">Your Consent</h3>

                        <p>
                            By registering on our Website or App, you give your express consent for the collection and use of your personal information by Kitsam.com and its partners and affiliates. From time to time, we may change our methods of collecting information and modify our Privacy Policy. We will post such changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it. Make sure that you stay updated on our Privacy Policy revisions.
                        </p>

                        <p>
                            Kitsan asserts that this Privacy Policy is only a description of its operation regarding user information. This Privacy Policy does not create any legal rights in your favour or in favour of any other person, group, or organization. Kitsam reserves the right to change this Privacy Policy at any time without prior notice.
                        </p>

                    </div>
               </div>
          </div>
        </div>
    </section>
</main>

<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-top">
        <div class="container">
            <div class="row  justify-content-center">
                <div class="col-lg-6">
                    <h3>KITSAM</h3>
                    <p>KITSAM is one of the most popular fantasy sports and fantasy cricket platforms all across the world. Our platform is one of the simplest to play on and the easiest ways to win real cash.</p>
                </div>
            </div>

            <div class="useful mt-5">
                <h4>Useful Links</h4>
                <div class="opt-inline">
                    <a href="/privacy-policy" class="pd">Privacy Policy</a><span>||</span>
                    <a href="/terms-of-use" class="pd">Terms of Use</a><span>||</span>
                    <a href="/refund-policy" class="pd">Refund Policy</a>
                </div>
            </div>

            <div class="social-links">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>

        </div>
    </div>

    <div class="container footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>Kitsam</span></strong> All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bocor-bootstrap-template-nice-animation/ -->
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

</body>

</html>
